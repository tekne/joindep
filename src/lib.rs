use elysees::Arc;
use fxhash::FxHashMap;
use std::borrow::Cow;
use std::fmt::{self, Debug, Formatter};
use std::ops::*;

/// A trait implemented by terms
//TODO: check whether a term is general
pub trait Value: Sized {
    /// Convert this value into a `TermId`
    fn into_id(self) -> TermId {
        TermId::from_direct(self.into_term())
    }
    /// Convert this value into a `Term`
    fn into_term(self) -> Term;
    /// Whether this value is *structurally* equal to another term *without* further normalization
    /// This does *not* take types into account!
    fn seq(&self, other: &TermId) -> bool;
    /// Get the type of this term
    fn ty(&self) -> BType<'_>;
    /// Check whether a term is a type
    fn is_ty(&self) -> bool;
    /// Get the universe of this term if it is a type, or `None` otherwise
    fn universe(&self) -> Option<Universe>;
    /// Get the universe of this term if it is a type, or `None` otherwise, normalizing as dictated by a context
    fn universe_with(&self, ctx: &mut EvalCtx) -> Result<Option<Universe>, Error> {
        let ty = self.ty();
        match ty {
            BType::Universe(universe) => Ok(Some(universe)),
            BType::Borrowed(ty) => match &**ty.eval_with(ctx)?.as_ref().unwrap_or(ty) {
                Term::Universe(universe) => Ok(Some(universe.clone())),
                _ => Ok(None),
            },
        }
    }
    /// Get the free variable bound of this term
    fn fvb(&self) -> u64;
    /// Evaluate this term within a given context, or `None` if none exists
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error>;
    /// Perform a single reduction of this term with a given reduction and termination condition
    fn redex_with(
        &self,
        reduction: Reduction,
        termination_condition: TerminationCondition,
    ) -> Result<Option<TermId>, Error> {
        let cfg = EvalCfg::single_step(reduction, termination_condition);
        self.eval_with(&mut cfg.ctx())
    }
    /// Substitute variable `n` of this term with a given value, or shift it away if `None`, leaving all variables with de-Bruijn index less than `n` unchanged.
    /// Return an error if there is an attempt to shift away an existing variable, or if there is an attempt to construct a malformed term.
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error>;
    /// Typecheck a term in a given context; return an error on failure
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error>;
    /// Typecheck a term; return an error on failure
    fn tyck(&self) -> Result<bool, Error> {
        self.tyck_with(&mut TyCtx::default())
    }
}

/// A term
#[derive(Clone, Eq, Hash)]
pub struct TermId(Arc<Term>);

impl TermId {
    /// Create a `TermId` directly from a `Term`
    pub fn from_direct(term: Term) -> TermId {
        TermId(Arc::new(term))
    }
    /// Evaluate this term within a given context, getting a `Cow`
    pub fn eval_with_or_cow(&self, ctx: &mut EvalCtx) -> Result<Cow<TermId>, Error> {
        self.eval_with(ctx)
            .map(|eval| eval.map(Cow::Owned).unwrap_or(Cow::Borrowed(self)))
    }
    /// Evaluate this term within a given context
    pub fn eval_with_or_clone(&self, ctx: &mut EvalCtx) -> Result<TermId, Error> {
        self.eval_with(ctx)
            .map(|eval| eval.unwrap_or_else(|| self.clone()))
    }
    /// Evaluate this term within a given context
    pub fn eval_with_or_take(self, ctx: &mut EvalCtx) -> Result<TermId, Error> {
        self.eval_with(ctx).map(|eval| eval.unwrap_or(self))
    }
    /// Substitute variable 0 of this term with a given value, or shift it away if `None`
    pub fn subst_var_or_clone(&self, term: Option<&TermId>, n: u64) -> Result<TermId, Error> {
        self.subst_var(term, n)
            .map(|subst| subst.unwrap_or_else(|| self.clone()))
    }
}

impl Debug for TermId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Debug::fmt(&self.0, f)
    }
}

impl Value for TermId {
    fn into_id(self) -> TermId {
        self
    }
    fn into_term(self) -> Term {
        (*self.0).clone()
    }
    fn seq(&self, other: &TermId) -> bool {
        self.ptr_eq(other) || self.0.seq(other)
    }
    fn ty(&self) -> BType<'_> {
        self.0.ty()
    }
    fn is_ty(&self) -> bool {
        self.0.is_ty()
    }
    fn universe(&self) -> Option<Universe> {
        self.0.universe()
    }
    fn fvb(&self) -> u64 {
        self.0.fvb()
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        self.0.eval_with(ctx)
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        self.0.subst_var(term, n)
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        self.0.tyck_with(ctx)
    }
}

impl TermId {
    /// Check two terms share the same storage
    pub fn ptr_eq(&self, other: &TermId) -> bool {
        Arc::ptr_eq(&self.0, &other.0)
    }
}

impl PartialEq for TermId {
    fn eq(&self, other: &TermId) -> bool {
        // Optimization: check for pointer equality first, since terms are `Eq`.
        Arc::ptr_eq(&self.0, &other.0) || self.0 == other.0
    }
}

impl Deref for TermId {
    type Target = Term;
    fn deref(&self) -> &Term {
        &*self.0
    }
}

/// An enumeration detailing possible terms
//TODO: path induction
#[derive(Clone, Eq, PartialEq, Hash)]
pub enum Term {
    /// A lambda function, which parametrizes a term with a variable having a given type
    Lambda(Lambda),
    /// A lambda function, which parametrizes a term with a variable having a given type
    Pi(Pi),
    /// An application of one term to another
    App(App),
    /// A simple computational proof that two terms are equal in a given type
    Refl(Refl),
    /// A path
    Path(Path),
    /// A variable, represented by a de-Bruijn index
    Var(Var),
    /// A typing universe
    Universe(Universe),
}

#[macro_export]
macro_rules! for_term {
    ($v:expr; $i:ident => $e:expr) => {
        match $v {
            Term::Lambda($i) => $e,
            Term::Pi($i) => $e,
            Term::App($i) => $e,
            Term::Refl($i) => $e,
            Term::Path($i) => $e,
            Term::Var($i) => $e,
            Term::Universe($i) => $e,
        }
    };
}

impl Debug for Term {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for_term!(self; v => Debug::fmt(v, f))
    }
}

impl Value for Term {
    fn into_id(self) -> TermId {
        for_term!(self; v => v.into_id())
    }
    fn into_term(self) -> Term {
        self
    }
    fn seq(&self, other: &TermId) -> bool {
        for_term!(self; v => v.seq(other))
    }
    fn ty(&self) -> BType<'_> {
        for_term!(self; v => v.ty())
    }
    fn is_ty(&self) -> bool {
        for_term!(self; v => v.is_ty())
    }
    fn universe(&self) -> Option<Universe> {
        for_term!(self; v => v.universe())
    }
    fn fvb(&self) -> u64 {
        for_term!(self; v => v.fvb())
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        for_term!(self; v => v.eval_with(ctx))
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        for_term!(self; v => v.subst_var(term, n))
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        for_term!(self; v => v.tyck_with(ctx))
    }
}

/// A potentially borrowed type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum BType<'a> {
    /// A borrowed term ID
    Borrowed(&'a TermId),
    /// A typing universe
    Universe(Universe),
}

impl Value for BType<'_> {
    fn into_id(self) -> TermId {
        match self {
            BType::Borrowed(ty) => ty.clone(),
            BType::Universe(u) => u.into_id(),
        }
    }
    fn into_term(self) -> Term {
        match self {
            BType::Borrowed(ty) => (**ty).clone(),
            BType::Universe(u) => Term::Universe(u),
        }
    }
    fn seq(&self, other: &TermId) -> bool {
        match self {
            BType::Borrowed(ty) => ty.seq(other),
            BType::Universe(u) => u.seq(other),
        }
    }
    fn ty(&self) -> BType<'_> {
        match self {
            BType::Borrowed(ty) => ty.ty(),
            BType::Universe(u) => u.ty(),
        }
    }
    fn is_ty(&self) -> bool {
        match self {
            BType::Borrowed(ty) => ty.is_ty(),
            BType::Universe(_u) => true,
        }
    }
    fn universe(&self) -> Option<Universe> {
        match self {
            BType::Borrowed(ty) => ty.universe(),
            BType::Universe(u) => u.universe(),
        }
    }
    fn fvb(&self) -> u64 {
        match self {
            BType::Borrowed(ty) => ty.fvb(),
            BType::Universe(u) => u.fvb(),
        }
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        match self {
            BType::Borrowed(ty) => ty.eval_with(ctx),
            BType::Universe(u) => u.eval_with(ctx),
        }
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        match self {
            BType::Borrowed(ty) => ty.subst_var(term, n),
            BType::Universe(_u) => Ok(None),
        }
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        match self {
            BType::Borrowed(ty) => ty.tyck_with(ctx),
            BType::Universe(_u) => Ok(true),
        }
    }
}

/// A lambda function, which parametrizes a term with a variable having a given type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Lambda {
    /// The type of the argument to this lambda function
    param: TermId,
    /// The result of this lambda function
    result: TermId,
    /// The type of this lambda function
    ty: TermId,
    /// The free variable bound of this lambda function
    fvb: u64,
}

impl Lambda {
    /// Construct a new lambda function
    pub fn try_new(param: TermId, result: TermId) -> Result<Lambda, Error> {
        Self::try_new_with(param, result, &mut EvalCtx::default())
    }
    /// Construct a new lambda function within a given context
    pub fn try_new_with(param: TermId, result: TermId, ctx: &mut EvalCtx) -> Result<Lambda, Error> {
        // NOTE: `param`, under suitable normalization, being a type, is checked by `Pi`'s constructor
        // Any transformation of `param` is similarly done by `Pi`'s constructor
        let ty = Pi::try_new_with(param, result.ty().into_id(), ctx)?;
        let param = ty.param.clone();
        let fvb = result.fvb().saturating_sub(1).max(param.fvb());
        debug_assert!(ty.fvb() <= fvb);
        let lambda = Lambda {
            param,
            result,
            ty: ty.into_id(),
            fvb,
        };
        Ok(lambda)
    }
    /// Apply this function to a term
    pub fn apply_lambda(&self, term: &TermId) -> Result<TermId, Error> {
        //TODO: check parameter type?
        self.result.subst_var_or_clone(Some(term), 0)
    }
    /// Get the type for a lambda function
    pub fn lambda_ty(param: &TermId, result: &TermId) -> Result<TermId, Error> {
        Pi::try_new(param.clone(), result.ty().into_id()).map(Value::into_id)
    }
}

impl Value for Lambda {
    fn into_term(self) -> Term {
        Term::Lambda(self)
    }
    fn seq(&self, other: &TermId) -> bool {
        match &**other {
            Term::Lambda(other) => self.result.seq(&other.result),
            _ => false,
        }
    }
    fn ty(&self) -> BType<'_> {
        BType::Borrowed(&self.ty)
    }
    fn is_ty(&self) -> bool {
        false
    }
    fn universe(&self) -> Option<Universe> {
        None
    }
    fn fvb(&self) -> u64 {
        self.fvb
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if ctx.is_null() {
            return Ok(None);
        }
        if ctx.config.eta {
            if let Term::App(app) = &*self.result {
                if let Term::Var(var) = &*app.right {
                    if var.ix == 0 {
                        if let Ok(result) = self.result.subst_var_or_clone(None, 0) {
                            ctx.reduce();
                            return result.eval_with_or_clone(ctx).map(Some);
                        }
                    }
                }
            }
        }
        let param = if ctx.config.reduce_types {
            self.param.eval_with(ctx)?
        } else {
            None
        };
        let result = if ctx.config.reduce_under_lambda {
            self.result.eval_with(ctx)?
        } else {
            None
        };
        let (param, result) = match (param, result) {
            (None, None) => return Ok(None),
            (Some(param), None) => (param, self.result.clone()),
            (None, Some(result)) => (self.param.clone(), result),
            (Some(param), Some(result)) => (param, result),
        };
        return Lambda::try_new(param, result).map(Value::into_id).map(Some);
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        if self.fvb <= n {
            Ok(None)
        } else if self.fvb == n + 1 && term.is_none() {
            Err(Error::InvalidShift)
        } else {
            let param = self.param.subst_var(term, n)?;
            let result = self.result.subst_var(term, n + 1)?;
            let (param, result) = match (param, result) {
                (None, None) => return Ok(None),
                (Some(param), None) => (param, self.result.clone()),
                (None, Some(result)) => (self.param.clone(), result),
                (Some(param), Some(result)) => (param, result),
            };
            return Lambda::try_new(param, result).map(Value::into_id).map(Some);
        }
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        debug_assert_eq!(
            Ok(&self.ty),
            Self::lambda_ty(&self.param, &self.result).as_ref()
        );
        if !self.param.tyck_with(ctx)? {
            return Ok(false);
        }
        if !self.ty.tyck_with(ctx)? {
            return Ok(false);
        }
        ctx.push();
        let result = self.result.tyck_with(ctx);
        ctx.pop();
        result
    }
}

/// A lambda function, which parametrizes a term with a variable having a given type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Pi {
    /// The type of the argument to this pi type
    param: TermId,
    /// The result type of this pi type
    result: TermId,
    /// The type of this pi type
    ty: Universe,
    /// The free variable bound of this pi type
    fvb: u64,
}

impl Pi {
    /// Try to construct a new pi type given a parameter type and a result type
    pub fn try_new(param: TermId, result: TermId) -> Result<Pi, Error> {
        Self::try_new_with(param, result, &mut EvalCtx::default())
    }
    /// Try to construct a new pi type given a parameter type and a result type, with a given evaluation context for types
    pub fn try_new_with(param: TermId, result: TermId, ctx: &mut EvalCtx) -> Result<Pi, Error> {
        let param_ty = param.universe_with(ctx)?.ok_or(Error::ExpectedType)?;
        let result_ty = result.universe_with(ctx)?.ok_or(Error::ExpectedType)?;
        let ty = param_ty.join(result_ty);
        let fvb = result.fvb().saturating_sub(1).max(param.fvb());
        let pi = Pi {
            param,
            result,
            ty,
            fvb,
        };
        Ok(pi)
    }
    /// Apply this dependent function type to a type, getting a type
    pub fn apply_pi(&self, term: &TermId) -> Result<TermId, Error> {
        //TODO: check parameter type?
        self.result.subst_var_or_clone(Some(term), 0)
    }
}

impl Value for Pi {
    fn into_term(self) -> Term {
        Term::Pi(self)
    }
    fn seq(&self, other: &TermId) -> bool {
        match &**other {
            Term::Pi(other) => self.param.seq(&other.param) && self.result.seq(&other.result),
            _ => false,
        }
    }
    fn ty(&self) -> BType<'_> {
        BType::Universe(self.ty.clone())
    }
    fn is_ty(&self) -> bool {
        true
    }
    fn universe(&self) -> Option<Universe> {
        Some(self.ty.clone())
    }
    fn fvb(&self) -> u64 {
        self.fvb
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if ctx.is_null() {
            return Ok(None);
        }
        let param = self.param.eval_with(ctx)?;
        let result = if ctx.config.reduce_under_pi {
            self.result.eval_with(ctx)?
        } else {
            None
        };
        let (param, result) = match (param, result) {
            (None, None) => return Ok(None),
            (Some(param), None) => (param, self.result.clone()),
            (None, Some(result)) => (self.param.clone(), result),
            (Some(param), Some(result)) => (param, result),
        };
        return Pi::try_new(param, result).map(Value::into_id).map(Some);
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        if self.fvb <= n {
            Ok(None)
        } else if self.fvb == n + 1 && term.is_none() {
            Err(Error::InvalidShift)
        } else {
            let param = self.param.subst_var(term, n)?;
            let result = self.result.subst_var(term, n + 1)?;
            let (param, result) = match (param, result) {
                (None, None) => return Ok(None),
                (Some(param), None) => (param, self.result.clone()),
                (None, Some(result)) => (self.param.clone(), result),
                (Some(param), Some(result)) => (param, result),
            };
            return Pi::try_new(param, result).map(Value::into_id).map(Some);
        }
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        let param_universe = self.param.universe().ok_or(Error::ExpectedType)?;
        let result_universe = self.result.universe().ok_or(Error::ExpectedType)?;
        if self.ty != param_universe.join(result_universe) {
            return Err(Error::InvalidAnnotation);
        }
        Ok(self.param.tyck_with(ctx)? && self.result.tyck_with(ctx)?)
    }
}

/// An application of one term to another
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct App {
    /// The term being applied
    left: TermId,
    /// The term the function is being applied to
    right: TermId,
    /// The type of this application
    ty: TermId,
    /// The free variable bound of this application
    fvb: u64,
}

impl App {
    /// Construct a new function application
    pub fn try_new(left: TermId, right: TermId) -> Result<App, Error> {
        Self::try_new_with(left, right, &mut EvalCtx::default())
    }
    /// Construct a new function application in a given context
    pub fn try_new_with(left: TermId, right: TermId, ctx: &mut EvalCtx) -> Result<App, Error> {
        //TODO: clean this up...
        let tmp_term;
        let pi = match left.ty() {
            BType::Borrowed(ty) => match &**ty {
                Term::Pi(pi) => pi,
                term => {
                    let mut reduce_under_pi = false;
                    std::mem::swap(&mut ctx.config.reduce_under_pi, &mut reduce_under_pi);
                    let term = term.eval_with(ctx);
                    tmp_term = term?.ok_or(Error::ExpectedFunction)?;
                    ctx.config.reduce_under_pi = reduce_under_pi;
                    match &*tmp_term {
                        Term::Pi(pi) => pi,
                        _ => return Err(Error::ExpectedFunction),
                    }
                }
            },
            BType::Universe(_) => return Err(Error::ExpectedFunction),
        };
        let ty = pi.apply_pi(&right)?;
        let fvb = left.fvb().max(right.fvb());
        debug_assert!(ty.fvb() <= fvb);
        let app = App {
            left,
            right,
            ty,
            fvb,
        };
        Ok(app)
    }
    /// Get the type of an application
    fn app_ty(left: &TermId, right: &TermId) -> Result<TermId, Error> {
        //TODO: reduction?
        let pi = match left.ty() {
            BType::Borrowed(ty) => match &**ty {
                Term::Pi(pi) => pi,
                _ => return Err(Error::ExpectedFunction),
            },
            _ => return Err(Error::ExpectedFunction),
        };
        pi.apply_pi(&right)
    }
}

impl Value for App {
    fn into_term(self) -> Term {
        Term::App(self)
    }
    fn seq(&self, other: &TermId) -> bool {
        match &**other {
            Term::App(other) => self.left.seq(&other.left) && self.right.seq(&other.right),
            _ => false,
        }
    }
    fn ty(&self) -> BType<'_> {
        BType::Borrowed(&self.ty)
    }
    fn is_ty(&self) -> bool {
        match &*self.ty {
            Term::Universe(_u) => true,
            _ => false,
        }
    }
    fn universe(&self) -> Option<Universe> {
        match &*self.ty {
            Term::Universe(u) => Some(u.clone()),
            _ => None,
        }
    }
    fn fvb(&self) -> u64 {
        self.fvb
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if ctx.is_null() {
            return Ok(None);
        }
        //TODO: weak normal for eta: do we go through App? What's more useful?
        let left = self.left.eval_with(ctx)?;
        if !ctx.is_null() && ctx.config.beta {
            if let Term::Lambda(lambda) = &**left.as_ref().unwrap_or(&self.left) {
                return if let Ok(term) = lambda.apply_lambda(&self.right) {
                    ctx.reduce();
                    Ok(Some(term.eval_with(ctx)?.unwrap_or(term)))
                } else {
                    //TODO: normalization errors, rather than `None` all the time?
                    Ok(None)
                };
            }
        }
        let right = self.right.eval_with(ctx)?;
        let (left, right) = match (left, right) {
            (None, None) => return Ok(None),
            (Some(left), None) => (left, self.right.clone()),
            (None, Some(right)) => (self.left.clone(), right),
            (Some(left), Some(right)) => (left, right),
        };
        App::try_new(left, right).map(Value::into_id).map(Some)
        /*
        //TODO: think about this...
        if ctx.config.termination_condition.ty {
            if let Some(ty) = self.ty.redex_with(ctx) {
                let app = App {
                    left: self.left.clone(),
                    right: self.right.clone(),
                    ty,
                    fvb: self.fvb,
                };
                return Some(app.into_id());
            }
        }
        None
        */
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        if self.fvb <= n {
            Ok(None)
        } else if self.fvb == n + 1 && term.is_none() {
            Err(Error::InvalidShift)
        } else {
            let left = self.left.subst_var(term, n)?;
            let right = self.right.subst_var(term, n + 1)?;
            let (left, right) = match (left, right) {
                (None, None) => return Ok(None),
                (Some(left), None) => (left, self.right.clone()),
                (None, Some(right)) => (self.left.clone(), right),
                (Some(left), Some(right)) => (left, right),
            };
            //TODO: potentially optimize here...
            return App::try_new(left, right).map(Value::into_id).map(Some);
        }
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        debug_assert_eq!(Ok(&self.ty), App::app_ty(&self.left, &self.right).as_ref());
        Ok(self.left.tyck_with(ctx)? && self.right.tyck_with(ctx)?)
    }
}

/// A simple computational proof that two terms are equal in a given type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Refl {
    /// The path being proved
    path: TermId,
}

impl Refl {
    /// Construct a path from a type to itself
    pub fn new_loop(a: TermId) -> Result<Refl, Error> {
        Self::try_new(a.clone(), a)
    }
    /// Construct a path from a type to itself
    pub fn new_loop_with(a: TermId, ctx: &mut EvalCtx) -> Result<Refl, Error> {
        Self::try_new_with(a.clone(), a, ctx)
    }
    /// Try to construct a path between two terms
    pub fn try_new(left: TermId, right: TermId) -> Result<Refl, Error> {
        Self::try_new_with(left, right, &mut EvalCtx::default())
    }
    /// Try to construct a path between two terms within a context
    pub fn try_new_with(left: TermId, right: TermId, ctx: &mut EvalCtx) -> Result<Refl, Error> {
        if left != right {
            //TODO: optimize, etc...
            if left.eval_with_or_cow(ctx)? != right.eval_with_or_cow(ctx)? {
                return Err(Error::ValueMismatch);
            }
        }
        let path = Path::try_new_with(left, right, ctx)?.into_id();
        Ok(Refl { path })
    }
}

impl Value for Refl {
    fn into_term(self) -> Term {
        Term::Refl(self)
    }
    fn seq(&self, other: &TermId) -> bool {
        match &**other {
            Term::Refl(_) => true,
            _ => false,
        }
    }
    fn ty(&self) -> BType<'_> {
        BType::Borrowed(&self.path)
    }
    fn is_ty(&self) -> bool {
        false
    }
    fn universe(&self) -> Option<Universe> {
        None
    }
    fn fvb(&self) -> u64 {
        self.path.fvb()
    }
    fn eval_with(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        // Reflexivity never normalizes, and two instances are always equal
        Ok(None)
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        if self.fvb() <= n {
            Ok(None)
        } else if self.fvb() == n + 1 && term.is_none() {
            Err(Error::InvalidShift)
        } else {
            let path = self.path.subst_var_or_clone(term, n)?;
            let refl = Refl { path };
            Ok(Some(refl.into_id()))
        }
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        //TODO: check the path itself is valid?
        self.path.tyck_with(ctx)
    }
}

impl Refl {
    //// The path being proved
    pub fn path(&self) -> &Path {
        match &*self.path {
            Term::Path(path) => path,
            _ => unreachable!("Invalid path in refl {:#?}", self),
        }
    }
}

/// The type of paths between two terms
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Path {
    //TODO: base type?
    /// The left term being checked
    left: TermId,
    /// The right term being checked
    right: TermId,
    /// The type of this path
    ty: Universe,
    /// The free variable bound of this path
    fvb: u64,
}

impl Path {
    /// Try to construct the path type between two terms
    pub fn try_new(left: TermId, right: TermId) -> Result<Path, Error> {
        Self::try_new_with(left, right, &mut EvalCtx::default())
    }
    /// Try to construct the path type between two terms within a context
    pub fn try_new_with(left: TermId, right: TermId, ctx: &mut EvalCtx) -> Result<Path, Error> {
        let left_ty = left.ty().universe_with(ctx)?.ok_or(Error::ExpectedType)?;
        let right_ty = right.ty().universe_with(ctx)?.ok_or(Error::ExpectedType)?;
        let ty = left_ty.join(right_ty);
        let fvb = left.fvb().max(right.fvb());
        let path = Path {
            left,
            right,
            ty,
            fvb,
        };
        Ok(path)
    }
}

impl Value for Path {
    fn into_term(self) -> Term {
        Term::Path(self)
    }
    fn ty(&self) -> BType<'_> {
        BType::Universe(self.ty.clone())
    }
    fn seq(&self, other: &TermId) -> bool {
        match &**other {
            Term::Path(other) => self.left.seq(&other.left) && self.right.seq(&other.right),
            _ => false,
        }
    }
    fn is_ty(&self) -> bool {
        true
    }
    fn universe(&self) -> Option<Universe> {
        Some(self.ty.clone())
    }
    fn fvb(&self) -> u64 {
        self.fvb
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if ctx.is_null() {
            return Ok(None);
        }
        let left = self.left.eval_with(ctx)?;
        let right = self.right.eval_with(ctx)?;
        let (left, right) = match (left, right) {
            (None, None) => return Ok(None),
            (Some(left), None) => (left, self.right.clone()),
            (None, Some(right)) => (self.left.clone(), right),
            (Some(left), Some(right)) => (left, right),
        };
        let fvb = left.fvb().max(right.fvb());
        let path = Path {
            left,
            right,
            ty: self.ty.clone(),
            fvb,
        };
        Ok(Some(path.into_id()))
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        if self.fvb <= n {
            Ok(None)
        } else if self.fvb == n + 1 && term.is_none() {
            Err(Error::InvalidShift)
        } else {
            let left = self.left.subst_var_or_clone(term, n)?;
            let right = self.right.subst_var_or_clone(term, n)?;
            let fvb = left.fvb().max(right.fvb());
            let path = Path {
                left,
                right,
                ty: self.ty.clone(),
                fvb,
            };
            Ok(Some(path.into_id()))
        }
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        let left_universe = self.left.universe().ok_or(Error::ExpectedType)?;
        let right_universe = self.right.universe().ok_or(Error::ExpectedType)?;
        if self.ty != left_universe.join(right_universe) {
            return Err(Error::InvalidAnnotation);
        }
        Ok(self.left.tyck_with(ctx)? && self.right.tyck_with(ctx)?)
    }
}

/// A variable, represented by a de-Bruijn index
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Var {
    /// The index of this variable
    ix: u64,
    /// The type of this variable
    ty: TermId,
    /// The free variable bound of this variable
    fvb: u64,
}

impl Var {
    /// Create a new variable with a given index and type *without* checking `ty` is a type
    pub fn new(ix: u64, ty: TermId) -> Var {
        let fvb = ty.fvb().max(ix + 1);
        Var { ix, ty, fvb }
    }
}

impl Value for Var {
    fn into_term(self) -> Term {
        Term::Var(self)
    }
    fn ty(&self) -> BType<'_> {
        BType::Borrowed(&self.ty)
    }
    fn seq(&self, other: &TermId) -> bool {
        match &**other {
            Term::Var(other) => self.ix == other.ix,
            _ => false,
        }
    }
    fn is_ty(&self) -> bool {
        match &*self.ty {
            Term::Universe(_u) => true,
            _ => false,
        }
    }
    fn universe(&self) -> Option<Universe> {
        match &*self.ty {
            Term::Universe(u) => Some(u.clone()),
            _ => None,
        }
    }
    fn fvb(&self) -> u64 {
        self.fvb
    }
    fn eval_with(&self, ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        if ctx.is_null() {
            return Ok(None);
        }
        if ctx.config.reduce_types {
            if let Some(ty) = self.ty.eval_with(ctx)? {
                let fvb = ty.fvb().max(self.ix + 1);
                let var = Var {
                    ix: self.ix,
                    ty,
                    fvb,
                };
                return Ok(Some(var.into_id()));
            }
        }
        Ok(None)
    }
    fn subst_var(&self, term: Option<&TermId>, n: u64) -> Result<Option<TermId>, Error> {
        //TODO: check substitution type?
        if self.ix == n {
            if let Some(term) = term {
                Ok(Some(term.clone()))
            } else {
                Err(Error::InvalidShift)
            }
        } else if self.fvb <= n {
            Ok(None)
        } else if self.fvb == n + 1 && term.is_none() {
            Err(Error::InvalidShift)
        } else {
            let ty = self.ty.subst_var_or_clone(term, n)?;
            let ix = self.ix - 1;
            let fvb = ty.fvb().max(ix);
            let var = Var { ix, ty, fvb };
            Ok(Some(var.into_id()))
        }
    }
    fn tyck_with(&self, ctx: &mut TyCtx) -> Result<bool, Error> {
        ctx.constrain(self.ix, &self.ty)
    }
}

/// A typing universe
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct Universe {
    /// The level of this universe
    level: u64,
}

impl Universe {
    /// Get the universe of sets
    pub fn set() -> Universe {
        Universe { level: 1 }
    }
    /// Get a universe containing this one
    pub fn succ(&self) -> Universe {
        Universe {
            level: self.level + 1,
        }
    }
    /// Get a universe containing this universe and another
    pub fn join(&self, other: Universe) -> Universe {
        Universe {
            level: self.level.max(other.level),
        }
    }
}

impl Debug for Universe {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Universe({})", self.level)
    }
}

impl Value for Universe {
    fn into_term(self) -> Term {
        Term::Universe(self)
    }
    fn seq(&self, other: &TermId) -> bool {
        match &**other {
            Term::Universe(other) => self.level == other.level,
            _ => false,
        }
    }
    fn ty(&self) -> BType<'_> {
        BType::Universe(self.succ())
    }
    fn is_ty(&self) -> bool {
        true
    }
    fn universe(&self) -> Option<Universe> {
        Some(self.succ())
    }
    fn fvb(&self) -> u64 {
        0
    }
    fn eval_with(&self, _ctx: &mut EvalCtx) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    fn subst_var(&self, _term: Option<&TermId>, _n: u64) -> Result<Option<TermId>, Error> {
        Ok(None)
    }
    fn tyck_with(&self, _ctx: &mut TyCtx) -> Result<bool, Error> {
        Ok(true)
    }
}

/// A reduction type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Reduction {
    /// Perform both beta reductions and eta reductions
    BetaEta,
    /// Perform beta reductions
    Beta,
    /// Peform eta reductions
    Eta,
}

impl Reduction {
    /// Whether eta reduction is allowed
    pub fn eta(&self) -> bool {
        matches!(self, Reduction::Eta | Reduction::BetaEta)
    }
    /// Whether beta reduction is allowed
    pub fn beta(&self) -> bool {
        matches!(self, Reduction::Beta | Reduction::BetaEta)
    }
}

/// A termination condition for reduction
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct TerminationCondition {
    /// The form to normalize values to
    form: Form,
    /// The form to normalize types to
    ty: bool,
}

/// Forms for reduction
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Form {
    /// Normal form
    Normal,
    /// Weak normal form
    WeakNormal,
}

/// A potential error
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Error {
    /// A type mismatch
    TypeMismatch,
    /// A value mismatch
    ValueMismatch,
    /// A type was expected
    ExpectedType,
    /// A function was expected
    ExpectedFunction,
    /// Attempting to shift a term with fvb 1
    InvalidShift,
    /// An invalid type annotation
    InvalidAnnotation,
}

impl From<Term> for TermId {
    fn from(term: Term) -> TermId {
        term.into_id()
    }
}

impl From<Lambda> for TermId {
    fn from(term: Lambda) -> TermId {
        term.into_id()
    }
}

impl From<Pi> for TermId {
    fn from(term: Pi) -> TermId {
        term.into_id()
    }
}

impl From<App> for TermId {
    fn from(term: App) -> TermId {
        term.into_id()
    }
}

impl From<Path> for TermId {
    fn from(term: Path) -> TermId {
        term.into_id()
    }
}

impl From<Refl> for TermId {
    fn from(term: Refl) -> TermId {
        term.into_id()
    }
}

impl From<Var> for TermId {
    fn from(term: Var) -> TermId {
        term.into_id()
    }
}

impl From<Universe> for TermId {
    fn from(term: Universe) -> TermId {
        term.into_id()
    }
}

impl From<Lambda> for Term {
    fn from(term: Lambda) -> Term {
        term.into_term()
    }
}

impl From<Pi> for Term {
    fn from(term: Pi) -> Term {
        term.into_term()
    }
}

impl From<App> for Term {
    fn from(term: App) -> Term {
        term.into_term()
    }
}

impl From<Path> for Term {
    fn from(term: Path) -> Term {
        term.into_term()
    }
}

impl From<Refl> for Term {
    fn from(term: Refl) -> Term {
        term.into_term()
    }
}

impl From<Var> for Term {
    fn from(term: Var) -> Term {
        term.into_term()
    }
}

impl From<Universe> for Term {
    fn from(term: Universe) -> Term {
        term.into_term()
    }
}

/// A type-checking context
#[derive(Debug, Clone, Default)]
pub struct TyCtx {
    /// The types of variables in this context
    var_types: FxHashMap<i64, TermId>,
    /// The shift for variables in this context
    shift: i64,
}

impl TyCtx {
    /// Remove a constraint on a variable to be a given type
    fn unconstrain(&mut self, ix: u64) {
        self.var_types.remove(&(ix as i64 + self.shift));
    }
    /// Constrain a variable be a given type.
    ///
    /// Return an error on failure to constrain for potentially non-type-erroneous reasons (e.g. timeout)
    /// Return `Ok(true)` on a successful constraint, and `Ok(false)` on a confirmed inconsistency
    pub fn constrain(&mut self, ix: u64, ty: &TermId) -> Result<bool, Error> {
        //TODO: attempted normalization, etc.
        if self
            .var_types
            .entry(ix as i64 + self.shift)
            .or_insert_with(|| ty.clone())
            != ty
        {
            Err(Error::TypeMismatch)
        } else {
            Ok(true)
        }
    }
    /// Check a variable's type is compatible with the given context
    pub fn check(&self, ix: i64, ty: &TermId) -> bool {
        //TODO: attempted normalization
        match self.var_types.get(&(ix as i64 + self.shift)) {
            Some(var_ty) => ty == var_ty,
            None => true,
        }
    }
    /// Push a layer onto the variable stack
    pub fn push(&mut self) {
        self.shift -= 1
    }
    /// Pop a layer from the variable stack
    pub fn pop(&mut self) {
        self.unconstrain(0);
        self.shift += 1
    }
}

/// An evaluation configuration
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct EvalCfg {
    /// Whether to reduce under lambda functions
    pub reduce_under_lambda: bool,
    /// Whether to reduce under pi types
    pub reduce_under_pi: bool,
    /// Whether to reduce types
    pub reduce_types: bool,
    /// Whether to perform beta reductions
    pub beta: bool,
    /// Whether to perform eta reductions
    pub eta: bool,
    /// The maximum number of reductions remaining for known-terminating terms
    pub max_bounded_reductions: u64,
    /// The maximum number of reductions remaining for *each* potentially non-terminating term
    pub max_general_reductions: u64,
}

impl EvalCfg {
    /// The configuration for a null reduction
    pub const NULL: EvalCfg = EvalCfg {
        reduce_under_lambda: false,
        reduce_under_pi: false,
        reduce_types: false,
        beta: false,
        eta: false,
        max_bounded_reductions: 0,
        max_general_reductions: 0,
    };
    /// The configuration for a single-step reduction
    pub fn single_step(
        reduction: Reduction,
        termination_condition: TerminationCondition,
    ) -> EvalCfg {
        EvalCfg {
            reduce_under_lambda: matches!(termination_condition.form, Form::Normal),
            reduce_under_pi: true,
            reduce_types: termination_condition.ty,
            beta: reduction.beta(),
            eta: reduction.eta(),
            max_bounded_reductions: 1,
            max_general_reductions: 1,
        }
    }
    /// Get a context for this reduction
    pub fn ctx(&self) -> EvalCtx {
        EvalCtx {
            config: self.clone(),
            bounded_reductions: 0,
            general_reductions: 0,
            general_term_depth: 0,
        }
    }
}

impl Default for EvalCfg {
    fn default() -> Self {
        Self::NULL
    }
}

/// A normalization context
#[derive(Debug, Clone, Default)]
pub struct EvalCtx {
    /// The normalization configuration for this context
    config: EvalCfg,
    /// The number of reductions
    bounded_reductions: u64,
    /// The number of general reductions performed
    general_reductions: u64,
    /// The general term depth encountered
    general_term_depth: u64,
}

impl EvalCtx {
    /// Whether this context is null, i.e. never peforms a redex
    pub fn is_null(&self) -> bool {
        self.bounded_reductions >= self.config.max_bounded_reductions
            || self.general_reductions >= self.config.max_general_reductions
    }
    /// Perform a reduction
    fn reduce(&mut self) {
        self.bounded_reductions += 1;
        if self.general_term_depth > 0 {
            self.general_reductions += 1;
        }
    }
    /// Push to the general term depth
    #[allow(dead_code)]
    fn push_general(&mut self) {
        debug_assert!(self.general_term_depth != 0 || self.general_reductions == 0);
        self.general_term_depth += 1
    }
    /// Pop from the general term depth
    #[allow(dead_code)]
    fn pop_general(&mut self) {
        self.general_term_depth -= 1;
        if self.general_term_depth == 0 {
            self.general_reductions = 0
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn basic_application() {
        let set = Universe::set().into_id();
        let x = Var::new(0, set.clone()).into_id();
        assert_eq!(x.tyck(), Ok(true));
        let y = Var::new(1, set.clone()).into_id();
        assert_eq!(y.tyck(), Ok(true));
        let id = Lambda::try_new(set.clone(), x.clone()).unwrap().into_id();
        assert_eq!(id.tyck(), Ok(true));
        let app_y = App::try_new(id.clone(), y.clone()).unwrap().into_id();
        let cfg = EvalCfg::single_step(
            Reduction::BetaEta,
            TerminationCondition {
                form: Form::WeakNormal,
                ty: false,
            },
        );
        assert_eq!(app_y.tyck(), Ok(true));
        let mut ctx = cfg.ctx();
        assert_eq!(app_y.eval_with(&mut ctx).unwrap().as_ref(), Some(&y));
        let app_x = App::try_new(id.clone(), x.clone()).unwrap().into_id();
        assert_eq!(app_x.tyck(), Ok(true));
        assert_eq!(app_x.eval_with(&mut cfg.ctx()).unwrap().as_ref(), Some(&x));
        let p2 = Lambda::try_new(set.clone(), id.clone()).unwrap().into_id();
        assert_eq!(p2.tyck(), Ok(true));
        let app2_y = App::try_new(p2.clone(), y.clone()).unwrap().into_id();
        assert_eq!(app2_y.tyck(), Ok(true));
        assert_eq!(
            app2_y.eval_with(&mut cfg.ctx()).unwrap().as_ref(),
            Some(&id)
        );
        let app2_x = App::try_new(p2.clone(), x.clone()).unwrap().into_id();
        assert_eq!(app2_y.tyck(), Ok(true));
        assert_eq!(
            app2_x.eval_with(&mut cfg.ctx()).unwrap().as_ref(),
            Some(&id)
        );
        let app2_xy = App::try_new(
            App::try_new(p2.clone(), x.clone()).unwrap().into_id(),
            y.clone(),
        )
        .unwrap()
        .into_id();
        assert_eq!(app2_xy.tyck(), Ok(true));
        assert_eq!(
            app2_xy.eval_with(&mut cfg.ctx()).unwrap().as_ref(),
            Some(&app_y)
        );
        let app2_yx = App::try_new(
            App::try_new(p2.clone(), y.clone()).unwrap().into_id(),
            x.clone(),
        )
        .unwrap()
        .into_id();
        assert_eq!(app2_yx.tyck(), Ok(true));
        assert_eq!(
            app2_yx.eval_with(&mut cfg.ctx()).unwrap().as_ref(),
            Some(&app_x)
        );
    }
    #[test]
    fn basic_refl() {
        //TODO: multi-step reduction
        //TODO: path induction
        let set = Universe::set().into_id();
        let x = Var::new(0, set.clone()).into_id();
        let y = Var::new(1, set.clone()).into_id();
        let id = Lambda::try_new(set.clone(), x.clone()).unwrap().into_id();
        let app_x = App::try_new(id.clone(), x.clone()).unwrap().into_id();
        let terms = [&x, &y, &id, &app_x];
        let cfg = EvalCfg::single_step(
            Reduction::BetaEta,
            TerminationCondition {
                form: Form::WeakNormal,
                ty: false,
            },
        );
        for &term in &terms {
            let refl = Refl::try_new(term.clone(), term.clone()).unwrap();
            assert_eq!(refl, Refl::new_loop(term.clone()).unwrap());
            assert_eq!(
                refl,
                Refl::new_loop_with(term.clone(), &mut cfg.ctx()).unwrap()
            );
            assert_eq!(
                refl,
                Refl::try_new_with(term.clone(), term.clone(), &mut cfg.ctx()).unwrap()
            );
        }
        assert_eq!(
            Refl::try_new(x.clone(), y.clone()),
            Err(Error::ValueMismatch)
        );
        assert_eq!(
            Refl::try_new_with(x.clone(), y.clone(), &mut cfg.ctx()),
            Err(Error::ValueMismatch)
        );
        assert_eq!(
            Refl::try_new(y.clone(), app_x.clone()),
            Err(Error::ValueMismatch)
        );
        assert_eq!(
            Refl::try_new_with(y.clone(), app_x.clone(), &mut cfg.ctx()),
            Err(Error::ValueMismatch)
        );
        assert_eq!(
            Refl::try_new(x.clone(), app_x.clone()),
            Err(Error::ValueMismatch)
        );
        Refl::try_new_with(x.clone(), app_x.clone(), &mut cfg.ctx()).unwrap();
    }
}
